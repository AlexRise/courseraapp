package by.mik.courseraapp;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;


public class MainActivity extends AppCompatActivity {

    private final static String DIALOG = "dialog";

    private SeekBar seekBar;
    private LinearLayout linLayoutPurple;
    private LinearLayout linLayoutTurquoise;
    private LinearLayout linLayoutGray;
    private LinearLayout linLayoutRed;
    private LinearLayout linLayoutGreen;
    private DialogFragment dialog;

    //MainActivity initialization
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dialog = new MomaDialog();

        //binding elements view with MainActivity
        linLayoutPurple = (LinearLayout) findViewById(R.id.layoutPurple);
        linLayoutTurquoise = (LinearLayout) findViewById(R.id.layoutTurquoise);
        linLayoutGray = (LinearLayout) findViewById(R.id.layoutGray);
        linLayoutRed = (LinearLayout) findViewById(R.id.layoutRed);
        linLayoutGreen = (LinearLayout) findViewById(R.id.layoutGreen);

        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);

        //the initial color setting
        updateBackground();
    }


    //color change depending on the position Seek Bar
    private void updateBackground() {
        int seekR = seekBar.getProgress();

        linLayoutPurple.setBackgroundColor(0xff000000 + 125 * 0x10000 + seekR * 0x100
                + 125);
        linLayoutTurquoise.setBackgroundColor(0xff000000 + seekR * 0x10000 + 200 * 0x100
                + 200);
        linLayoutGray.setBackgroundColor(0xff000000 + (140 + seekR / 3) * 0x10000 + (140 + seekR / 3) * 0x100
                + (140 + seekR / 3));
        linLayoutRed.setBackgroundColor(0xff000000 + 255 * 0x10000 + seekR * 0x100
                + 0);
        linLayoutGreen.setBackgroundColor(0xff000000 + 0 * 0x10000 + 255 * 0x100
                + seekR);
    }

    //create options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //to determine which button was pressed for OptionsMenu
        switch (item.getItemId()) {
            case R.id.more_info:
                dialog.show(getFragmentManager(), DIALOG);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //implements SeekBar.OnSeekBarChangeListener
    private SeekBar.OnSeekBarChangeListener seekBarChangeListener =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress,
                                              boolean fromUser) {
                    updateBackground();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };
}
