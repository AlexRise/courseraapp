package by.mik.courseraapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

public class MomaDialog extends DialogFragment implements OnClickListener {

    private final static String URI_MOMA = "http://www.moma.org";

    //create custom dialog
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        builder.setPositiveButton(R.string.visit_moma, this);
        builder.setNegativeButton(R.string.not_now, this);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog, null);

        builder.setView(view);
        return builder.create();
    }

    //implemented method onClick
    @Override
    public void onClick(DialogInterface dialog, int which) {

        //to determine which button was pressed for Dialog
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(URI_MOMA));
                startActivity(intent);
                break;
            case Dialog.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
        }
    }
}
